# README

API reference is [here](http://docs.blazingseoproxyresellerapi.apiary.io) (latest stable)

## Example

Easiest way to create proxies:

```
#!php
<?php

use Blazing\Reseller\Api;

// API Configuration
$apiConfig = (new Api\BlazingProxiesConfig())
  ->setApiToken('YOUR-API-TOKEN')
  // required as well
  ->setLogPath(__DIR__ . '/blazing-api.log');

$apiFactory = new Api\BlazingProxiesAdapter($apiConfig);
$api = $apiFactory->getUser();

// Create new user record
$response = $api->add('customerUsername', true);

$proxyUserId = $response[ 'user_id' ];

// Create proxy
$response = $api->updatePlan($proxyUserId, [
    'country'    => $country,
    'category'   => $category,
    'count'      => 5,
    'expiration' => date('Y-m-d H:i:s', strtotime('+1 month', time()))
]);

// Set proxy locations
$response = $api->saveLocationPreference($proxyUserId, $country, $category, [
    'locations' => [$location => 5]
]);

// Get user and assigned proxies (be noticed that proxies assigned after a some time)
$userData = $api->get($proxyUserId);

// You can do anything you want here: output somewhere, save to db, email customer
var_dump($userData);

```