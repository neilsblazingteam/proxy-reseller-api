<?php

namespace Blazing\Reseller\Api;

use ErrorException;

class BlazingProxiesConfig
{

    protected $protocol = 'http';
    protected $host = 'www.blazingseollc.com';
    protected $url = '/api/proxy';
    protected $logPath;
    protected $apiToken;

    /**
     * Get protocol
     *
     * @return string
     * @throws ErrorException
     */
    public function getProtocol()
    {
        if (!$this->protocol) {
            throw new ErrorException('Protocol is not configured!');
        }

        return $this->protocol;
    }

    /**
     * Set protocol
     *
     * @param string $protocol
     * @return $this
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;

        return $this;
    }

    /**
     * Get host
     *
     * @return string
     * @throws ErrorException
     */
    public function getHost()
    {
        if (!$this->host) {
            throw new ErrorException('Host is not configured!');
        }

        return $this->host;
    }

    /**
     * Set host
     *
     * @param string $host
     * @return $this
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     * @throws ErrorException
     */
    public function getUrl()
    {
        if (!$this->url) {
            throw new ErrorException('Url is not configured!');
        }

        return $this->url;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get logPath
     *
     * @return mixed
     * @throws ErrorException
     */
    public function getLogPath()
    {
        if (!$this->logPath) {
            throw new ErrorException('Log path is not configured!');
        }

        return $this->logPath;
    }

    /**
     * Set logPath

     *
*@param mixed $logPath
     * @return $this
     */
    public function setLogPath($logPath)
    {
        $this->logPath = $logPath;

        return $this;
    }

    /**
     * Get apiToken
     *
     * @return mixed
     * @throws ErrorException
     */
    public function getApiToken()
    {
        if (!$this->apiToken) {
            throw new ErrorException('API token is not configured!');
        }

        return $this->apiToken;
    }

    /**
     * Set apiToken
     *
     * @param mixed $apiToken
     * @return $this
     */
    public function setApiToken($apiToken)
    {
        $this->apiToken = $apiToken;

        return $this;
    }


}
